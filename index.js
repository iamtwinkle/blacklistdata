const redis = require("redis");
const fs = require("fs");
const path = require("path");

const insertData = async () => {
  try {
    const client = redis.createClient({
      host: "127.0.0.1",
      port: 6379,
      //   password: ""/,
      database: "1",
    });

    await client.connect();

    client.on("error", (err) => console.log("Redis Client Error", err));

    client.on("connect", (msg) => console.log("Redis Client connect", msg));

    fs.readdir("domain", function (err, list) {
      console.log(list);
      if (err) {
        return err;
      }
      list.forEach(function (file) {
        console.log(file);
        fs.readdir(`domain/${file}`, function (err, lists) {
         
          if (err) {
            console.log(err);
            return err;
          }

          lists.forEach(function (files) {
            file = path.resolve(`domain/${file}`, files);

            fs.stat(file, function (err, stat) {
              if (stat && stat.isDirectory()) {
                recursiveSearch(file);
              } else {
                fs.readFile(file, { encoding: "utf-8" }, function (err, data) {
                  if (!err) {
                    const domainData = data.toString().split("\n");
                    console.log("received data: ", data);

                    for (var i = 0; i < domainData.length; i++) {
                      client.set(domainData[i], domainData[i]).then((res) => {
                        // console.log(res);
                      });
                    }
                  } else {
                    console.log(err);
                  }
                });
              }
            });
          });
        });
      });
    });;
  } catch (err) {
    console.log(err);
  }
};

insertData();
